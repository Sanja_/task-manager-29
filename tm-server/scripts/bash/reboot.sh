!#/user/bin/env bash

echo 'REBOOT TM-SERVER'

if [ ! -f tm-server.pid ]; then
	echo "Task-server pid not found!"
	exit 1;
fi

echo 'KILL PROCESS WITH PID '$(cat tm-server.pid);
kill -9 $(cat ./tm-server.pid)
rm ./tm-server.pid

mkdir -p ../logs
rm -f ../logs/tm-server.log
nohup java -jar ../../tm-server.jar> ../logs/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo "task-manager is running with pid "$!