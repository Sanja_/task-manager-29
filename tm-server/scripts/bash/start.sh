!#/user/bash

if [ -f ./tm-server.pid ]; then
	echo "Task-manager server already started"
	exit 1;
fi

mkdir -p ../logs
rm -f ../logs/tm-server.log
nohup java -jar ../../tm-server.jar > ../logs/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo "task-manager is running with pid "$!


