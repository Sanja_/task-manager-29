!#/bin/bash

if [ ! -f tm-server.pid ]; then
	echo "server pid not found"
	exit 1;
fi

echo 'KILL PROCESS WITH PID '$(cat tm-server.pid);
kill -9 $(cat ./tm-server.pid)
rm ./tm-server.pid
echo 'OK';
