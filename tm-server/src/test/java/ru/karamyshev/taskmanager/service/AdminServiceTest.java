/*
package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.karamyshev.taskmanager.api.service.IAdminService;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;
import ru.karamyshev.taskmanager.constant.DataConstant;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.marker.ServerTestCategory;

import java.io.File;

@Category(ServerTestCategory.class)
public class AdminServiceTest {

    private final static IServiceLocator serviceLocator = new Bootstrap();

    private final static IAdminService adminService = new AdminService(serviceLocator);

    @BeforeClass
    public static void init() {
        final User user = serviceLocator.getUserService().create("user-1", "user-2", "user-1@.com");
        final User admin = serviceLocator.getUserService()
                .create("admin-1", "admin-2", "admin-1@com");
        final String adminId = String.valueOf(admin.getId());
        Assert.assertNotNull(user);
        final String userId = String.valueOf(user.getId());
        serviceLocator.getProjectService().create(userId, "project-1");
        serviceLocator.getProjectService().create(userId, "project-2");
        serviceLocator.getTaskService().create(adminId, "task-1");
        serviceLocator.getTaskService().create(adminId, "task-2");
    }

    @AfterClass
    public static void clearDataClass() throws Exception {
        adminService.clearBase64();
        adminService.clearFasterJson();
        adminService.clearFasterXml();
        adminService.clearJaxBJson();
        adminService.clearJaxBXml();
        adminService.clearBinary();
    }

    @Test
    public void saveBinaryTest() throws Exception {
        Assert.assertNotNull(serviceLocator);
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getProjectService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getTaskService().getList().size(), 2);
        adminService.saveBinary();
        @NotNull final File file = new File(DataConstant.FILE_BINARY);
        Assert.assertNotNull(file);

        Assert.assertNotNull(serviceLocator);
        final IServiceLocator testServiceLocator = new Bootstrap();
        testServiceLocator.getAdminService().loadBinary();
        Assert.assertEquals(testServiceLocator.getUserService().getList().size(),
                serviceLocator.getUserService().getList().size());
        Assert.assertEquals(testServiceLocator.getTaskService().getList().size(),
                serviceLocator.getTaskService().getList().size());
        Assert.assertEquals(testServiceLocator.getProjectService().getList().size(),
                serviceLocator.getProjectService().getList().size());
    }

    @Test
    public void saveBase64Test() throws Exception {
        Assert.assertNotNull(serviceLocator);
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getProjectService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getTaskService().getList().size(), 2);
        adminService.saveBase64();
        @NotNull final File file = new File(DataConstant.FILE_BASE64);
        Assert.assertNotNull(file);

        Assert.assertNotNull(serviceLocator);
        final IServiceLocator testServiceLocator = new Bootstrap();
        testServiceLocator.getAdminService().loadBase64();
        Assert.assertEquals(testServiceLocator.getUserService().getList().size(),
                serviceLocator.getUserService().getList().size());
        Assert.assertEquals(testServiceLocator.getTaskService().getList().size(),
                serviceLocator.getTaskService().getList().size());
        Assert.assertEquals(testServiceLocator.getProjectService().getList().size(),
                serviceLocator.getProjectService().getList().size());
    }

    @Test
    public void saveFasterJsonTest() throws Exception {
        Assert.assertNotNull(serviceLocator);
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getProjectService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getTaskService().getList().size(), 2);
        adminService.saveFasterJson();
        @NotNull final File file = new File(DataConstant.FILE_FAST_JSON);
        Assert.assertNotNull(file);

        Assert.assertNotNull(serviceLocator);
        final IServiceLocator testServiceLocator = new Bootstrap();
        testServiceLocator.getAdminService().loadFasterJson();
        Assert.assertEquals(testServiceLocator.getUserService().getList().size(),
                serviceLocator.getUserService().getList().size());
        Assert.assertEquals(testServiceLocator.getTaskService().getList().size(),
                serviceLocator.getTaskService().getList().size());
        Assert.assertEquals(testServiceLocator.getProjectService().getList().size(),
                serviceLocator.getProjectService().getList().size());
    }

    @Test
    public void saveFasterXmlTest() throws Exception {
        Assert.assertNotNull(serviceLocator);
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getProjectService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getTaskService().getList().size(), 2);
        adminService.saveFasterXml();
        @NotNull final File file = new File(DataConstant.FILE_FAST_XML);
        Assert.assertNotNull(file);


        Assert.assertNotNull(serviceLocator);
        final IServiceLocator testServiceLocator = new Bootstrap();
        testServiceLocator.getAdminService().loadFasterXml();
        Assert.assertEquals(testServiceLocator.getUserService().getList().size(),
                serviceLocator.getUserService().getList().size());
        Assert.assertEquals(testServiceLocator.getUserService().getList().size(),
                serviceLocator.getUserService().getList().size());
        Assert.assertEquals(testServiceLocator.getTaskService().getList().size(),
                serviceLocator.getTaskService().getList().size());
        Assert.assertEquals(testServiceLocator.getProjectService().getList().size(),
                serviceLocator.getProjectService().getList().size());
    }

    @Test
    public void saveJaxBJsonTest() throws Exception {
        Assert.assertNotNull(serviceLocator);
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getProjectService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getTaskService().getList().size(), 2);
        adminService.saveJaxBJson();
        @NotNull final File file = new File(DataConstant.FILE_JAX_JSON);
        Assert.assertNotNull(file);

        Assert.assertNotNull(serviceLocator);
        final IServiceLocator testServiceLocator = new Bootstrap();
        testServiceLocator.getAdminService().loadJaxBJson();
        Assert.assertEquals(testServiceLocator.getUserService().getList().size(),
                serviceLocator.getUserService().getList().size());
        Assert.assertEquals(testServiceLocator.getTaskService().getList().size(),
                serviceLocator.getTaskService().getList().size());
        Assert.assertEquals(testServiceLocator.getProjectService().getList().size(),
                serviceLocator.getProjectService().getList().size());
    }

    @Test
    public void saveJaxBXmlTest() throws Exception {
        Assert.assertNotNull(serviceLocator);
        Assert.assertEquals(serviceLocator.getUserService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getProjectService().getList().size(), 2);
        Assert.assertEquals(serviceLocator.getTaskService().getList().size(), 2);
        adminService.saveJaxBXml();
        @NotNull final File file = new File(DataConstant.FILE_JAX_XML);
        Assert.assertNotNull(file);

        Assert.assertNotNull(serviceLocator);
        final IServiceLocator testServiceLocator = new Bootstrap();
        testServiceLocator.getAdminService().loadJaxBXml();
        Assert.assertEquals(testServiceLocator.getUserService().getList().size(),
                serviceLocator.getUserService().getList().size());
        Assert.assertEquals(testServiceLocator.getTaskService().getList().size(),
                serviceLocator.getTaskService().getList().size());
        Assert.assertEquals(testServiceLocator.getProjectService().getList().size(),
                serviceLocator.getProjectService().getList().size());
    }

}
*/
