package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IRepository;
import ru.karamyshev.taskmanager.api.service.IService;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.entity.AbstractEntity;

import java.util.List;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
