package ru.karamyshev.taskmanager.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.service.IAdminService;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.constant.DataConstant;
import ru.karamyshev.taskmanager.dto.Domain;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class AdminService implements IAdminService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public AdminService(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void loadBinary() throws Exception {
        @NotNull final FileInputStream fileInputStream = new FileInputStream(DataConstant.FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        objectInputStream.close();
    }

    @Override
    public void saveBinary() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = createSaveFile(DataConstant.FILE_BINARY);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    public void clearBinary() throws Exception {
        clearSaveFile(DataConstant.FILE_BINARY);
    }

    @Override
    public void loadBase64() throws Exception {
        @NotNull final String dataBase64 = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_BASE64)));
        @NotNull final byte[] data = Base64.getDecoder().decode(dataBase64);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(data);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        objectInputStream.close();
    }

    @Override
    public void saveBase64() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = createSaveFile(DataConstant.FILE_BASE64);
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
        byteArrayOutputStream.close();
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void clearBase64() throws Exception {
        clearSaveFile(DataConstant.FILE_BASE64);
    }

    @Override
    public void loadFasterJson() throws Exception {
        @NotNull final String data = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_FAST_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void saveFasterJson() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = createSaveFile(DataConstant.FILE_FAST_JSON);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(domain);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void clearFasterJson() throws Exception {
        clearSaveFile(DataConstant.FILE_FAST_JSON);
    }

    @Override
    public void loadFasterXml() throws Exception {
        @NotNull final String data = new String(Files.readAllBytes(Paths.get(DataConstant.FILE_FAST_XML)));

        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);

        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void saveFasterXml() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = createSaveFile(DataConstant.FILE_FAST_XML);

        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(domain);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public void clearFasterXml() throws Exception {
        clearSaveFile(DataConstant.FILE_FAST_XML);
    }

    @Override
    public void loadJaxBJson() throws Exception {
        @NotNull final File file = new File(DataConstant.FILE_JAX_JSON);

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller un = jaxbContext.createUnmarshaller();

        un.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        un.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);

        @NotNull final Domain domain = (Domain) un.unmarshal(file);
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void saveJaxBJson() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = createSaveFile(DataConstant.FILE_JAX_JSON);

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller mar = jaxbContext.createMarshaller();

        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        mar.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        mar.marshal(domain, file);
    }

    @Override
    public void clearJaxBJson() throws Exception {
        clearSaveFile(DataConstant.FILE_JAX_JSON);
    }

    @Override
    public void loadJaxBXml() throws Exception {
        @NotNull final File file = new File(DataConstant.FILE_JAX_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller un = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) un.unmarshal(file);
        serviceLocator.getDomainService().load(domain);
    }

    @Override
    public void saveJaxBXml() throws Exception {
        @NotNull final Domain domain = prepareData();
        @NotNull final File file = createSaveFile(DataConstant.FILE_JAX_XML);

        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller mar = context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(domain, file);
    }

    @Override
    public void clearJaxBXml() throws Exception {
        clearSaveFile(DataConstant.FILE_JAX_XML);
    }

    @NotNull
    @Override
    public File createSaveFile(final String fileName) throws Exception {
        @NotNull final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        return file;
    }

    @Override
    public void clearSaveFile(@Nullable final String fileName) throws Exception {
        if (fileName == null || fileName.isEmpty()) return;
        final File file = new File(fileName);
        Files.deleteIfExists(file.toPath());
    }

    @NotNull
    private Domain prepareData() {
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainService().export(domain);
        return domain;
    }

}
