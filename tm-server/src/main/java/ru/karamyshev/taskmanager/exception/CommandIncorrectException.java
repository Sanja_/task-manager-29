package ru.karamyshev.taskmanager.exception;

public class CommandIncorrectException extends RuntimeException {

    public CommandIncorrectException(final String command) {
        super("Error! This command ``" +command+"`` is incorrect.");
    }

}
