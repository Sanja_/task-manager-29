package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.IUserRepository;
import ru.karamyshev.taskmanager.entity.User;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private EntityManager entityManager;

    public UserRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public List<User> findAll() {
        @Nullable final TypedQuery<User> typedQuery = entityManager.createQuery(
                "WHERE FROM User", User.class);
        if (typedQuery == null) return null;
        return typedQuery.getResultList();
    }

    @Nullable
    @Override
    public User add(@NotNull final User user) {
        entityManager.persist(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @Nullable final TypedQuery<User> typedQuery = entityManager.createQuery(
                "SELECT u FROM User u WHERE login = :login", User.class);
        typedQuery.setParameter("login", login);
        typedQuery.setMaxResults(1);
        return typedQuery.getSingleResult();
    }

    @Nullable
    @Override
    public User removeUser(@NotNull final User user) {
        entityManager.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final String id) {
        final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Override
    public void removeAllUsers() {
        entityManager.createQuery("DELETE FROM User")
                .executeUpdate();
    }

}
