package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.repository.ISessionRepository;
import ru.karamyshev.taskmanager.entity.Session;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Session add(@NotNull final Session session) {
        entityManager.persist(session);
        return session;
    }

    @Override
    public void remove(@NotNull final Session session) {
        entityManager.remove(session);
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        @Nullable final TypedQuery<Session> typedQuery = entityManager.createQuery(
                "FROM Session", Session.class);
        return typedQuery.getResultList();
    }

    @Nullable
    @Override
    public List<Session> findByUserId(@Nullable final String userId) throws Exception {
        @Nullable final TypedQuery<Session> typedQuery = entityManager.createQuery(
                "FROM Session WHERE user_id = :userId", Session.class)
                .setParameter("userId", userId);
        return typedQuery.getResultList();
    }

    @Nullable
    @Override
    public Session findById(@Nullable final String id) {
        System.out.println( "MANAGER " + entityManager.toString());
        return entityManager.find(Session.class, id);
    }

    @Nullable
    @Override
    public void removeByUserId(@Nullable final String userId){
        System.out.println("userId " + userId);
        entityManager.createQuery(
                "DELETE Session WHERE user_id = :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean contains(@Nullable final String id) {
        @Nullable final Session session = findById(id);
        return findAll().contains(session);
    }

}
