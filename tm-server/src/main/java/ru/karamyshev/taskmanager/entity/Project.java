package ru.karamyshev.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project")
public class Project extends AbstractEntity {

    private static final long serialVersionUID = 1001L;

    @NotNull
    @Column(
            columnDefinition = "TINYTEXT",
            nullable = false
    )
    private String name = "";

    @Nullable
    @Column(columnDefinition = "TEXT")
    private String description = "";

    @Nullable
    private Date startData;

    @Nullable
    private Date finishData;

    @Nullable
    @Column(columnDefinition = "DATETIME", updatable = false)
    private Date creationData = new Date(System.currentTimeMillis());

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @OneToMany(
            mappedBy = "project",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

}
