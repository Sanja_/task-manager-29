package ru.karamyshev.taskmanager.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@NoArgsConstructor
@Table(name = "app_session")
public class Session extends AbstractEntity {

    @Nullable
    @Column(
            columnDefinition = "TEXT",
            updatable = false
    )
    private Long startTime;

    @Nullable
    @ManyToOne
    private User user;

    @Nullable
    @Column(
            columnDefinition = "TEXT",
            updatable = false
    )
    private String signature;

}
