package ru.karamyshev.taskmanager.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.endpoint.IUserEndpoint;
import ru.karamyshev.taskmanager.api.service.IServiceLocator;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.dto.UserDTO;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class UserEndpoint implements IUserEndpoint {

    @NotNull
    private IServiceLocator serviceLocator;

    public UserEndpoint() {
    }

    public UserEndpoint(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password
    ) throws Exception {
        serviceLocator.getUserService().create(login, password);
    }

    @Override
    @WebMethod
    public void createUserEmail(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "email", partName = "email") @Nullable String email
    ) throws Exception {
        serviceLocator.getUserService().create(login, password, email);
    }

    @Nullable
    @Override
    @WebMethod
    public void createUserRole(
            @WebParam(name = "login", partName = "login") @Nullable String login,
            @WebParam(name = "password", partName = "password") @Nullable String password,
            @WebParam(name = "role", partName = "role") @Nullable Role role
    ) throws Exception {
        serviceLocator.getUserService().create(login, password, role);
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public User findUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByLogin(login);
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "user", partName = "user") @Nullable User user
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().removeUser(user);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().removeById(session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public void removeByLogin(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Nullable
    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "login", partName = "login") @Nullable String login
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        @Nullable final User currentUser = serviceLocator.getUserService().findById(session.getUserId());
        serviceLocator.getUserService().removeUserByLogin(currentUser.getLogin(), login);
    }

    @Nullable
    @Override
    @WebMethod
    public List<UserDTO> getUserList(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    public void renamePassword(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "newPassword", partName = "newPassword") @Nullable final String newPassword
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService()
                .renamePassword(session.getUserId(), newPassword);
    }

    @Override
    @WebMethod
    public void loadUser(
            @WebParam(name = "users", partName = "users") @Nullable List<User> users,
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session);
        //  serviceLocator.getUserService().load(users);
    }

}
