package ru.karamyshev.taskmanager.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(@NotNull Project project);

    @Nullable List<Project> findAllByUserId(@NotNull String userId);

    void clear(@NotNull String userId);

    @Nullable Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable Project removeOneById(@NotNull String userId, @NotNull String id);

    @Nullable Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable Project removeOneByName(@NotNull String userId, @NotNull String name);

    void removeAll();

}
