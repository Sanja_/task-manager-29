package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.TaskDTO;
import ru.karamyshev.taskmanager.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {


    void create(
            @Nullable String userId,
            @Nullable String taskName,
            @Nullable String projectName
    ) throws Exception;

    void create(
            @Nullable String userId,
            @Nullable String taskName,
            @Nullable String projectName,
            @Nullable String description
    ) throws Exception;

    void remove(@Nullable Task task);

    @Nullable List<TaskDTO> findAllByUserId(@Nullable String userId);

    void clearTaskByUserId(@Nullable String userId);

    @NotNull Task findOneByName(
            @Nullable String userId,
            @Nullable String name
    );

    @Nullable Task findOneById(@Nullable String userId, @Nullable String id);

    @Nullable List<TaskDTO> findAll();

    void updateTaskById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws Exception;

     void removeOneByName(@Nullable String userId, @Nullable String name);

     void removeOneById(@Nullable String userId, @Nullable String id);

     void removeAll();
}
