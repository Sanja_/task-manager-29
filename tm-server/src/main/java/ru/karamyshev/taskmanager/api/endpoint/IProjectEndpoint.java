package ru.karamyshev.taskmanager.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.ProjectDTO;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.entity.Project;
import ru.karamyshev.taskmanager.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {


    @WebMethod
    void createNameProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> findAllProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

    @WebMethod
    void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String name
    ) throws Exception;

    @Nullable
    @WebMethod
    void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String id,
            @WebParam(name = "projectUpName", partName = "projectUpName") @Nullable String name,
            @WebParam(name = "projectUpDescription", partName = "projectUpDescription") @Nullable String description
    ) throws Exception;

    @Nullable
    @WebMethod
    void removeOneProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "projectName", partName = "projectName") @Nullable String name
    ) throws Exception;

    @Nullable
    @WebMethod
    Project findOneProjectById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    void removeOneProjectById(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    List<ProjectDTO> getProjectList(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session
    ) throws Exception;

    @WebMethod
    void loadProject(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "projects", partName = "projects") @Nullable List<Project> projects
    ) throws Exception;

    @WebMethod
    void createProjectAndDescription(
            @WebParam(name = "session", partName = "session") @Nullable SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable String name,
            @WebParam(name = "description", partName = "description") @Nullable String description
    ) throws Exception;
}
