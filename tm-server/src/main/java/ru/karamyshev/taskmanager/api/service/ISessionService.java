package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.dto.SessionDTO;
import ru.karamyshev.taskmanager.dto.UserDTO;
import ru.karamyshev.taskmanager.entity.Session;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.util.List;

public interface ISessionService extends IService<Session> {

    void close(SessionDTO sessionDTO) throws Exception;

    void remove(@Nullable Session session) throws Exception;

    void closeAll(SessionDTO session) throws Exception;

    @Nullable UserDTO getUser(@NotNull SessionDTO session) throws Exception;

    @Nullable String getUserId(SessionDTO session) throws Exception;

    @Nullable List<SessionDTO> getListSessionUser(SessionDTO session) throws Exception;

    boolean isValid(SessionDTO session);

    void validate(@Nullable SessionDTO session) throws Exception;

    void validate(
            @Nullable SessionDTO session,
            @Nullable Role role
    ) throws Exception;

    @Nullable SessionDTO open(
            @Nullable String login,
            @Nullable String password
    ) throws Exception;

    @Nullable String sign(@Nullable SessionDTO session);

    boolean checkDataAccess(
            @Nullable String login,
            @Nullable String password
    ) throws Exception;

    void signOutByLogin(@Nullable String login) throws Exception;

    void signOutByUserId(@Nullable String userId) throws Exception;

}
