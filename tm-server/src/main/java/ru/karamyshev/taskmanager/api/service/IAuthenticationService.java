package ru.karamyshev.taskmanager.api.service;

import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.entity.User;
import ru.karamyshev.taskmanager.enumerated.Role;

public interface IAuthenticationService {

    @Nullable
    String getUserId();

    void checkRoles(@Nullable Role[] roles);

    @Nullable
    String getCurrentLogin();

    boolean isAuth();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    void registry(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    void renameLogin(
            @Nullable String userId,
            @Nullable String currentLogin,
            @Nullable String newLogin
    );

    @Nullable
    User showProfile(@Nullable String userId, @Nullable String login);

    void renamePassword(
            @Nullable String userId,
            @Nullable String currentLogin,
            @Nullable String oldPassword,
            @Nullable String newPassword
    );

}
