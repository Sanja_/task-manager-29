package ru.karamyshev.taskmanager;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.bootstrap.Bootstrap;

public class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }
 
}
