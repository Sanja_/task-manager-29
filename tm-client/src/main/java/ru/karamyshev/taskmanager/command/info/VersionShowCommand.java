package ru.karamyshev.taskmanager.command.info;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;

public class VersionShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @Override
    public @NotNull String description() {
        return "Show version info.";
    }

    @Override
    public void execute() {
        System.out.println("\n [VERSION]");
        System.out.println("1.0.8");
    }

}
