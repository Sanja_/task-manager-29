package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class RenameUserPasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-rnm-psswrd";
    }

    @NotNull
    @Override
    public String name() {
        return "rename-password";
    }

    @Override
    public @NotNull String description() {
        return "Rename password account.";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("CHANGE ACCOUNT PASSWORD");
        System.out.println("[ENTER NEW PASSWORD]");
        final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().renamePassword(session, newPassword);
        System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
