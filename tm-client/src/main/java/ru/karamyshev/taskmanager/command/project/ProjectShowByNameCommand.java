package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Project;
import ru.karamyshev.taskmanager.endpoint.ProjectDTO;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.util.List;

public class ProjectShowByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtvwnm";
    }

    @NotNull
    @Override
    public String name() {
        return "project-view-by-name";
    }

    @Override
    public @NotNull String description() {
        return "Show project by name.";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findOneProjectByName(session, name);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }

            showProjects(project);

        System.out.println("[OK]");
    }

    private void showProjects(@Nullable final Project project) {
        if (project == null) return;
        System.out.println("ID:" + project.getId());
        System.out.println("NAME:" + project.getName());
        System.out.println("DESCRIPTION:" + project.getDescription());
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
