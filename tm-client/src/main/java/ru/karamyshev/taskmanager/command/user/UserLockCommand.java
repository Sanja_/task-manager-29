package ru.karamyshev.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-lckusr";
    }

    @NotNull
    @Override
    public String name() {
        return "lock-user";
    }

    @Override
    public @NotNull String description() {
        return "Locked users";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[LOCK USER]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getAdminUserEndpoint().lockUser(session, login);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}
