package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.lang.Exception;

public class ProjectRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtrmvid";
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT ID FOR DELETION:");
        @Nullable final String idProject = TerminalUtil.nextLine();
       serviceLocator.getProjectEndpoint().removeOneProjectById(session, idProject);

         System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
