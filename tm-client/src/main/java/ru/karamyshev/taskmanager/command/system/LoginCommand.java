package ru.karamyshev.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;
import ru.karamyshev.taskmanager.exception.empty.LoginFailedException;
import ru.karamyshev.taskmanager.util.TerminalUtil;


public class LoginCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-lgin";
    }

    @NotNull
    @Override
    public String name() {
        return "login";
    }

    @Override
    public @NotNull String description() {
        return "Login in account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD");
        @Nullable final String password = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = serviceLocator.getSessionEndpoint().openSession(login, password);
        if (session == null) throw new LoginFailedException();
        serviceLocator.getSessionService().setSession(session);
        System.out.println("[OK]");
    }

}
