package ru.karamyshev.taskmanager.command.task;


import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.lang.Exception;


public class TaskRemoveByIdCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-tskrmvid";
    }

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    public @NotNull String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER TASK ID FOR DELETION:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getTaskEndpoint().removeTaskOneById(session, id);
      /*  if (task == null) System.out.println("[FAIL]");*/
         System.out.println("[OK]");
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
