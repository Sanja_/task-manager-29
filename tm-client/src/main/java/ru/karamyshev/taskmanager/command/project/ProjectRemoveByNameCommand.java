package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.*;
import ru.karamyshev.taskmanager.util.TerminalUtil;

import java.lang.Exception;
import java.util.List;


public class ProjectRemoveByNameCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtrmvnm";
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER PROJECT NAME FOR DELETION:");
       @Nullable final String name = TerminalUtil.nextLine();
       serviceLocator.getProjectEndpoint().removeOneProjectByName(session, name);

         System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
