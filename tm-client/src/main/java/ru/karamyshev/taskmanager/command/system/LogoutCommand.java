package ru.karamyshev.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;

public class LogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-lgout";
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Override
    public @NotNull String description() {
        return "Logout in account.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOGOUT]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getSessionEndpoint().closeSession(session);
        serviceLocator.getSessionService().clearSession();
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
