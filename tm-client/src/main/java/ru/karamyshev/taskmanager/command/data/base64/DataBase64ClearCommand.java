package ru.karamyshev.taskmanager.command.data.base64;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;

import java.io.Serializable;

public class DataBase64ClearCommand extends AbstractDataCommand implements Serializable {

    @NotNull
    @Override
    public String arg() {
        return "-dtbsclr";
    }

    @NotNull
    @Override
    public String name() {
        return "data-base64-clear";
    }

    @Override
    public @NotNull String description() {
        return "Remove base64 file.";
    }


    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[REMOVE BASE64 FILE]");
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminEndpoint().clearDataBase64(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
