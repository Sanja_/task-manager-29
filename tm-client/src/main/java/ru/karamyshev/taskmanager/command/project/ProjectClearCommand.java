package ru.karamyshev.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.command.AbstractCommand;
import ru.karamyshev.taskmanager.endpoint.Role;
import ru.karamyshev.taskmanager.endpoint.Session;
import ru.karamyshev.taskmanager.endpoint.SessionDTO;

public class ProjectClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-prtclr";
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public @NotNull String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("[CLEAR PROJECT]");
        serviceLocator.getProjectEndpoint().clearProject(session);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
