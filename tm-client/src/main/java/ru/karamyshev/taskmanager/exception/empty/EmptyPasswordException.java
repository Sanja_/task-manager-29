package ru.karamyshev.taskmanager.exception.empty;

public class EmptyPasswordException extends RuntimeException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}
