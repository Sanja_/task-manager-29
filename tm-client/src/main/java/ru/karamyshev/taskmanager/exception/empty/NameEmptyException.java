package ru.karamyshev.taskmanager.exception.empty;

public class NameEmptyException extends RuntimeException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }

}
