package ru.karamyshev.taskmanager.service;


import org.jetbrains.annotations.Nullable;
import ru.karamyshev.taskmanager.api.ICommandRepository;
import ru.karamyshev.taskmanager.api.ICommandService;
import ru.karamyshev.taskmanager.command.AbstractCommand;

import java.util.List;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Nullable
    public List<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
